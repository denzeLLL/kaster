import {Routes, RouterModule} from '@angular/router';
import {BookListComponent} from "./pages/book-list/book-list.component";
import {BookListEditRemoveComponent} from "./pages/book-list-edit-remove/book-list-edit-remove.component";
import {BookAddComponent} from "./pages/book-add/book-add.component";
import {BookReviewComponent} from "./pages/book-review/book-review.component";

const routes:Routes = [
    {
        path: '',
        redirectTo: 'book-add',
        pathMatch: 'full'
    },
    {
        path: 'book-list',
        component: BookListComponent,
    },
    {
        path: 'book-list-edit',
        component: BookListEditRemoveComponent
    },
    {
        path: 'book-add',
        component: BookAddComponent
    },
    {
        path: 'book-edit/:title',
        component: BookAddComponent
    },
    {
        path: 'book/:title', 
        component: BookReviewComponent
    },
];

export const BookRoutes = RouterModule.forChild(routes);

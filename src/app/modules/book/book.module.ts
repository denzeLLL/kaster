import {NgModule} from '@angular/core';
import {BookRoutes} from "./book-routing.module";
import {SharedModule} from "../../shared/shared.module";
import {BookListComponent} from './pages/book-list/book-list.component';
import {BookListEditRemoveComponent} from './pages/book-list-edit-remove/book-list-edit-remove.component';
import {BookAddComponent} from './pages/book-add/book-add.component';
import { BookReviewComponent } from './pages/book-review/book-review.component';

@NgModule({
    declarations: [
        BookListComponent,
        BookListEditRemoveComponent,
        BookAddComponent,
        BookReviewComponent
    ],
    imports: [
        BookRoutes,

        SharedModule
    ]
})
export class BookModule {
}

import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import {ValidationService} from "../../../../shared/services/validation.service";
import {StorageService} from "../../../../core/services/storage.service";
import {ActivatedRoute,Params,Router} from "@angular/router";

@Component({
    selector: 'app-book-add',
    templateUrl: './book-add.component.html',
    styleUrls: ['./book-add.component.scss']
})
export class BookAddComponent implements OnInit {

    public bookForm:FormGroup;
    public title: String;
    public edit: Boolean = false;
    public book = null;
    @ViewChild('fakeImg') fakeImg;
    @ViewChild('fakeCanvas') fakeCanvas;

    constructor(private fb:FormBuilder,
                private router: Router,
                private storageService:StorageService,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        let self = this;
        self.bookForm = this.fb.group({
            title: ['', [Validators.required, Validators.maxLength(30)]],
            imgStr: [''],
            authors: this.fb.array([this.buildAuthors()]),
            pages: ['', [Validators.required, ValidationService.min(0), ValidationService.max(10000)]],
            label: ['', [Validators.maxLength(30)]],
            published: ['', [ValidationService.yearFrom(1800)]],
            editionDate: [''],
            isbn: ['']
        });

        this.activatedRoute.params.forEach((params: Params) => {
            this.title = params["title"];
            this.edit = !!(params["title"]);
            this.buildForm();
        });

    }

    buildForm() {
        let self = this;

        if (self.edit) {
            this.storageService
                .getBook(this.title).subscribe(function (book) {
                self.book = book;
                self.bookForm.patchValue(book);
                for (let i = 0; i < book.authors.length; i++) {
                    if (i === 0) {
                        self.authors.at(i).patchValue(book.authors[i]);
                    } else {
                        self.addAuthor();
                        self.authors.at(i).patchValue(book.authors[i]);
                    }
                }
            });

        }
    }

    get f() {
        return this.bookForm.controls;
    }

    get authors():FormArray {
        return <FormArray>this.bookForm.get('authors');
    }

    handleLoadLocalFile(event) {
        event.preventDefault();

        let self = this,
            input = event.srcElement;

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                self.fakeImg.nativeElement.setAttribute('src', e.target['result']);
                setTimeout(function () {
                    self.storeImage();
                });
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    storeImage() {
        let self = this,
            canvas = self.fakeCanvas.nativeElement,
            img = self.fakeImg.nativeElement;
        canvas.setAttribute('width', img.width);
        canvas.setAttribute('height', img.height);

        let context = canvas.getContext('2d');
        context.drawImage(img, 0, 0);
        canvas.style.width = '100%';
        let data = canvas.toDataURL('image/png');
        self.bookForm.get('imgStr').setValue(data);
    }

    buildAuthors():FormGroup {
        return this.fb.group({
            firstName: ['', [Validators.required, Validators.maxLength(20)]],
            lastName: ['', Validators.maxLength(20)]
        });
    }


    addAuthor():void {
        this.authors.push(this.buildAuthors());
    }

    save() {
        let self = this;
        if (!self.edit) {
            console.log(self.bookForm);
            self.storageService.saveBook({book: self.bookForm.value})
                .subscribe(function (data) {
                        self.router.navigate(['book-list']);
                    },
                    function (err) {
                        console.log('err: ', err);
                    });
        } else {
            self.storageService.editBook({book: self.bookForm.value, title: self.title})
                .subscribe(function (data) {
                        self.router.navigate(['book-list']);
                    },
                    function (err) {
                        console.log('err: ', err);
                    });
        }

    }
}


/*
 •	Отображать список книг со следующими параметрами:
 - заголовок (обязательный параметр, не более 30 символов)
 - список авторов (книга должна содержать хотя бы одного автора)
 - имя автора (обязательный параметр, не более 20 символов)
 - фамилия автора (обязательный параметр, не более 20 символов)
 - количество страниц (обязательный параметр, больше 0 и не более 10000)
 - название издательства (опциональный параметр, не более 30 символов)
 - год публикации (опциональный параметр, не раньше 1800)
 - дата выхода в тираж (опциональный параметр, не раньше 01.01.1800)
 - ISBN с валидацией (опциональный параметр, http://en.wikipedia.org/wiki/International_Standard_Book_Number)
 - изображение (опциональный параметр)
 •	Возможность добавлять, удалять и редактировать существующие книги и ее авторов.
 •	Возможность сортировать по заголовку и году публикации (сортировка должна сохраняться после перезагрузки страницы)
 •	Возможность загружать изображение
 */
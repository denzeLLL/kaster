import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {ControlMessagesComponent} from "./components/control-messages/control-messages.component";

@NgModule({
    declarations: [
        ControlMessagesComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,

        ControlMessagesComponent
    ]
})
export class SharedModule {
}

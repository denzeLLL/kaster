import {Injectable} from '@angular/core';
import {Observable, Observer} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor() {}

    saveBook({book = null} = {}) {
        return Observable.create((observer: Observer<any>) => {
            let booksArr = JSON.parse(localStorage.getItem('books')) || [];
            if (!Array.isArray(booksArr)) {
                booksArr = [];
            }

            let bookIsset = booksArr.find(function (el) {
                if (el.title === book.title) {
                    return true
                }
            });
            
            if (bookIsset) {
                observer.error('Книга уже есть в базе.');
                return;
            }
            
            booksArr.push(book);
            localStorage.setItem('books', JSON.stringify(booksArr));
            observer.next(booksArr);
        });
    }


    editBook({book = null, title = ''}: {book?: any, title?: any} = {}) {
        return Observable.create((observer: Observer<any>) => {

            let booksArr = JSON.parse(localStorage.getItem('books')) || [];
            let isset = false;
            if (!Array.isArray(booksArr)) {
                booksArr = [];
            }

            let books = booksArr.map(function (el) {
                if (el.title === title) {
                    isset = true;
                    return book;
                } else {
                    return el;
                }
            });

            if (isset) {
                localStorage.setItem('books', JSON.stringify(books));
                observer.next(books);
            } else {
                observer.error('Такая книга не найдена.');
            }

        });
    }

    getBooks() {
        return Observable.create((observer: Observer<any>) => {
            let booksArr = JSON.parse(localStorage.getItem('books')) || [];
            if (!Array.isArray(booksArr)) {
                booksArr = [];
            }
            observer.next(booksArr);
        });
    }

    getBook(title) {
        return Observable.create((observer: Observer<any>) => {
            let booksArr = JSON.parse(localStorage.getItem('books')) || [];
            if (!Array.isArray(booksArr)) {
                booksArr = [];
            }

            let book = booksArr.find(function (el) {
                if (el.title === title) {
                    return true
                }
            });
            if (book) {
                observer.next(book);
            } else {
                observer.error('Такая книга не найдена.');
            }

        });
    }

    deleteBook(title) {
        return Observable.create((observer: Observer<any>) => {
            let booksArr = JSON.parse(localStorage.getItem('books')) || [];
            if (!Array.isArray(booksArr)) {
                booksArr = [];
            }
            booksArr = booksArr.filter(function(book) {
                return book.title !== title;
            });
            localStorage.setItem('books', JSON.stringify(booksArr));
            observer.next(booksArr);
        });
    }
}

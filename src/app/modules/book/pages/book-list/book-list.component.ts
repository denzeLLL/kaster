import {Component, OnInit} from '@angular/core';
import {StorageService} from "../../../../core/services/storage.service";

@Component({
    selector: 'app-book-list',
    templateUrl: './book-list.component.html',
    styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {

    public books:Array<any>;

    constructor(private storageService:StorageService) {

        let self = this;
        self.books = self.storageService
            .getBooks();
    }

    ngOnInit() {
    }

    deleteBook(title) {
        this.books = this.storageService.deleteBook(title);
    }
}

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ContentLayoutComponent} from "./layouts/content-layout/content-layout.component";

const routes:Routes = [
    {
        path: '',
        component: ContentLayoutComponent,
        children: [
            // {
            //     path: '',
            //     redirectTo: 'book',
            //     pathMatch: 'full'
            // },
            {
                path: '',
                loadChildren: './modules/book/book.module#BookModule'
            }
        ]
    },
    {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

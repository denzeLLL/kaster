import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookListEditRemoveComponent } from './book-list-edit-remove.component';

describe('BookListEditRemoveComponent', () => {
  let component: BookListEditRemoveComponent;
  let fixture: ComponentFixture<BookListEditRemoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookListEditRemoveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListEditRemoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

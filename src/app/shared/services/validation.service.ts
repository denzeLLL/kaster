import { Injectable } from '@angular/core';
import {ValidatorFn} from "@angular/forms";
import {ValidationErrors} from "@angular/forms";
import {AbstractControl} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

    public static getValidationErrorMessage(validatorName: string,
                                            validatorValue?: any,
                                            labelName?: string,
                                            patternText?: string): any {
        const config = {
            required: `Пое обязательно.`,
            maxlength: `Поле может содержать не более ${validatorValue.requiredLength} символов.`,
            minlength: `Поле может содержать не менее ${validatorValue.requiredLength} символов.`,
            min: `Количество не может быть менее ${validatorValue.min}`,
            max: `Количество не может быть больше ${validatorValue.max}`,
            yearFrom: `Год публикации не может быть раньше ${validatorValue.year} г.`,
            pattern: `${patternText}`
        };

        return config[validatorName];
    }

    static min(min: number): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            const value = parseFloat(control.value);
            return !isNaN(value) && value < min ? {'min': {'min': min, 'actual': control.value}} : null;
        };
    }

    static max(max: number): ValidatorFn {
        return (control:AbstractControl):ValidationErrors | null => {
            const value = parseFloat(control.value);
            return !isNaN(value) && value > max ? {'max': {'max': max, 'actual': control.value}} : null;
        };
    }

    static yearFrom(year: number): ValidatorFn {
        return (control:AbstractControl):ValidationErrors | null => {
            const value = parseFloat(control.value);
            return !isNaN(value) && value < year ? {'yearFrom': {'year': year, 'actual': control.value}} : null;
        };
    }
    
}

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {StorageService} from "../../../../core/services/storage.service";

@Component({
    selector: 'app-book-review',
    templateUrl: './book-review.component.html',
    styleUrls: ['./book-review.component.scss']
})
export class BookReviewComponent implements OnInit {


    public book;

    constructor(private route: ActivatedRoute, private storageService: StorageService) {
        this.route.params.subscribe(params => {
            let title = params['title'];
            if (title) {
                this.book = this.storageService
                    .getBook(title);
            }

        });
    }

    ngOnInit() {
    }

}
